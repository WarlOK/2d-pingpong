﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour 
{
	public int playerScore;
	public int player2Score;
	public GUIStyle guiStyle;

	void Start () 
	{
	
	}
	

	void Update () 
	{
	
	}

	void OnGUI()
	{
		GUI.Label(new Rect(Screen.width/2 - 150, 50, 50, 50), "" + playerScore, guiStyle);
		GUI.Label(new Rect(Screen.width/2 + 150, 50, 50, 50), "" + player2Score, guiStyle);
	}
}
