﻿using UnityEngine;
using System.Collections;

public class WindSnake : MonoBehaviour 
{
	
		public Color c1 = Color.yellow;
		public Color c2 = Color.red;
		public int lengthOfLineRenderer = 20;
		//public int i;
		//public int x;
	 	public LineRenderer lineRenderer;
		void Start() 
		{
			//LineRenderer lineRenderer = gameObject.AddComponent<LineRenderer>();
			//lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
			lineRenderer.SetColors(c1, c2);
			lineRenderer.SetWidth(0.3F, 0.3F);
			lineRenderer.SetVertexCount(lengthOfLineRenderer);
			lineRenderer.useWorldSpace = false;
			//LineRenderer lineRenderer = gameObject.GetComponent<LineRenderer>();
			//lineRenderer.SetVertexCount(lengthOfLineRenderer);
		}
		
		void Update() 
		{
			LineRenderer lineRenderer = GetComponent<LineRenderer>();
			int i = 0;
			
			while (i < lengthOfLineRenderer) {
				Vector3 pos = new Vector3(i * 1.4F, Mathf.Cos(i + Time.time), 0);
				lineRenderer.SetPosition(i, pos);
				i++;
				
			}
		}

}
