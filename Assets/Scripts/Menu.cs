﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour 
{
	private int _window = 0;
	public GUIStyle style;
	private bool enemyBot = false;

	void OnGUI ()
	{
		if (_window == 0)
		{
			if (GUI.Button(new Rect(Screen.width/2 - 100, 200, 200, 50), "New Game"))
			{
				_window = 1;
			}
			if (GUI.Button(new Rect(Screen.width/2 - 100, 300, 200, 50), "Exit"))
			{
				Application.Quit();
			}
		}

		if (_window == 1)
		{
			if (GUI.Button(new Rect(Screen.width/2 - 100, 200, 200, 50), "Start"))
			{
				Application.LoadLevel(1);
			}
			GUI.Label(new Rect(Screen.width/2 - 25, 250, 50, 50), "Enemy", style);
			GUI.Label(new Rect(Screen.width/2 - 100, 285, 50, 50), "Player");
			enemyBot = GUI.Toggle(new Rect(Screen.width/2 - 35, 285, 35, 35), enemyBot, "");
			GUI.Label(new Rect(Screen.width/2 + 40, 285, 50, 50), "Bot");
			enemyBot = GUI.Toggle(new Rect(Screen.width/2 + 85, 285, 35, 35), !enemyBot, "");

			if (GUI.Button(new Rect(Screen.width/2 - 100, 380, 200, 50), "Back") || Input.GetKeyDown("escape"))
			{
				_window = 0;
			}

			if (enemyBot)
			{
				PlayerPrefs.SetInt("EnemyType", 1);
			}
			else
			{
				PlayerPrefs.SetInt("EnemyType", 0);
			}
		}
	}


}
