﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour 
{
	int rand; 
	float timeout;	//для бага с стопором мяча посередине
	float timeout2; //для бага с застреванием позади игрока и частым отбиванием. Не используется
	public float speed = 100;
	//private float spd;

	public AudioClip hit1;
	public AudioClip hit2;

	void Start () 
	{
		Invoke("ForceExit", 1);
	}

	void ResetBall()
	{
		transform.position = new Vector3(0,0,0); 
		rigidbody2D.velocity = new Vector2(0, 0);
		Invoke("ForceExit", 0.7f);
	}
	
	void ForceExit() //Выход силы!
	{
		rand = Random.Range(0,2);
		if (rand == 0)
			rigidbody2D.AddForce(new Vector2(-speed, Random.Range(-30, 30)));
		if (rand == 1)
			rigidbody2D.AddForce(new Vector2(speed, Random.Range(-30, 30)));
	}

	void Update () 
	{
		Vector2 velX = rigidbody2D.velocity;
		print(velX);
		timeout += Time.deltaTime;
		if (timeout > 7)
		{
			timeout = 0;
			ResetBall();
		}
		//timeout2 += Time.deltaTime;
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.collider.tag == "Player")
		{
			timeout = 0;

			float velX = rigidbody2D.velocity.x;
			float velY = rigidbody2D.velocity.y;
			velY = velY/2 + col.collider.rigidbody2D.velocity.y/2;
			if (velX > 0)
				velX = velX + 0.2f;
			if (velX < 0)
			    velX = velX - 0.2f;

			rigidbody2D.velocity = new Vector2(velX, velY);
			//timeout2
			//spd = velY;
		}
		int rndAU = Random.Range(0,2);
		audio.pitch = Random.Range(0.7f, 1.2f);
		if (rndAU == 0)
			audio.clip = hit1;
			else 
			audio.clip = hit2;
		audio.Play();
	}
}
