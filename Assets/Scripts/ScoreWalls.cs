﻿using UnityEngine;
using System.Collections;

public class ScoreWalls : MonoBehaviour 
{
	public bool mainPlayerWall;
	public Score _score;
	public AudioClip win;
	public AudioClip lose;

	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.transform.CompareTag("Ball"))
			if(mainPlayerWall)
			{
				_score.player2Score ++;
				audio.clip = lose;
				audio.Play();
			}
				
			else
			{
				_score.playerScore ++;
				audio.clip = win;
				audio.Play();
			}
		col.gameObject.SendMessage("ResetBall");
	}

	void Start () 
	{
	
	}
	

	void Update () 
	{
	
	}
}
