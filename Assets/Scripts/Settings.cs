﻿using UnityEngine;
using System.Collections;

public class Settings : MonoBehaviour 
{
	public Camera cam;

	public BoxCollider2D TopWall;
	public BoxCollider2D RightWall;
	public BoxCollider2D LeftWall;
	public BoxCollider2D BottomWall;

	public Transform player1;
	public Transform player2;

	void Start () 
	{
		TopWall.size = new Vector2(cam.ScreenToWorldPoint(new Vector3(Screen.width*2, 0,0)).x,0.1f);
		TopWall.center = new Vector2(0, cam.ScreenToWorldPoint(new Vector3(0, Screen.height, 0)).y);

		BottomWall.size = new Vector2(cam.ScreenToWorldPoint(new Vector3(Screen.width*2, 0,0)).x,0.1f);
		BottomWall.center = new Vector2(0, cam.ScreenToWorldPoint(new Vector3(0, 0, 0)).y);

		LeftWall.size = new Vector2(0.1f, cam.ScreenToWorldPoint(new Vector3(0,Screen.height*2,0)).y);
		LeftWall.center = new Vector2(cam.ScreenToWorldPoint(new Vector3(0,0,0)).x, 0);

		RightWall.size = new Vector2(0.1f, cam.ScreenToWorldPoint(new Vector3(0,Screen.height*2,0)).y);
		RightWall.center = new Vector2(cam.ScreenToWorldPoint(new Vector3(Screen.width,0,0)).x, 0);

		Vector3 pl1pos = player1.position;
		pl1pos.x = cam.ScreenToWorldPoint(new Vector3(35, 0, 0)).x;
		player1.position = pl1pos;

		Vector3 pl2pos = player2.position;
		pl2pos.x = cam.ScreenToWorldPoint(new Vector3(Screen.width -35, 0, 0)).x;
		player2.position = pl2pos;

		if (PlayerPrefs.GetInt("EnemyType")==0)
		{
			player2.GetComponent<Player>().enabled = true;
		}
		if (PlayerPrefs.GetInt("EnemyType")==1)
		{
			player2.GetComponent<Bot>().enabled = true;
		}
	}
}
