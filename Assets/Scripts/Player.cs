﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
	public float speed = 11.0f;
	public KeyCode Up;
	public KeyCode Down;
	float _timeout;
	public bool mouseControl;

	void Update () 
	{
		Vector2 vect = rigidbody2D.velocity; //Управление клавой
		if (!mouseControl)
		{
			if (Input.GetKey(Up))
			{
				vect.y = speed;
				rigidbody2D.velocity = vect;
			}
			else if (Input.GetKey(Down))
			{
				vect.y = -speed;
				rigidbody2D.velocity = vect;
			}
			else 
			{
				vect.y = 0;
				rigidbody2D.velocity = vect;
			}
		}
		else									 //Управление мышью
			if (Input.GetAxis("Mouse Y") > 0)
			{
				_timeout = 0;
				vect.y = speed;
				rigidbody2D.velocity = vect;
			}
			else if (Input.GetAxis("Mouse Y") < 0)
			{
				_timeout = 0;
				vect.y = -speed;
				rigidbody2D.velocity = vect;
			}
			else 
			{
				_timeout += Time.deltaTime;
				if (_timeout > 0.06f)
				{
					_timeout = 0;
					vect.y = 0;
					rigidbody2D.velocity = vect;
				}
			}
	}
}
