﻿using UnityEngine;
using System.Collections;

public class Bot : MonoBehaviour 
{
	public Transform ball;
	public float speed;
	private float difference;

	void Start () 
	{
	
	}
	

	void Update () 
	{
		if(ball!=null)
		   {
			difference = transform.position.y - ball.position.y;
			Vector2 v = rigidbody2D.velocity;
			if (difference < -0.1f || difference > 0.1f)
			{
				if (transform.position.y < ball.position.y)
				{
					v.y = speed;
					rigidbody2D.velocity = v;
				}
				else if (transform.position.y > ball.position.y)
				{
					v.y = -speed;
					rigidbody2D.velocity = v;
				}
			}
			else 
			{
				v.y = 0;
				rigidbody2D.velocity = v;
			}
		}
	}
}
